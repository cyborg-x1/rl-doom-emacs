#!/bin/sh

name=`basename $1`
newsession=`qdbus org.kde.yakuake /yakuake/sessions org.kde.yakuake.addSession`
qdbus org.kde.yakuake /yakuake/tabs org.kde.yakuake.setTabTitle "$newsession" "Emacs: $name" && \
    qdbus org.kde.yakuake /yakuake/sessions org.kde.yakuake.runCommand "cd $1 && clear"
qdbus org.kde.yakuake /yakuake/window org.kde.yakuake.toggleWindowState
