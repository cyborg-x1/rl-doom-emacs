(
 (nil . (
         (indent-tabs-mode . t)
         (tab-width . 4)
         (cmake-ide-build-dir . "build")
		 (mode . irony)
		 (exe . "build/__PROJECT-NAME__")
		 )
      )

 (c-mode .
         (
          (c-file-style . "rl-std-cpp-style")
          )
         )
)


