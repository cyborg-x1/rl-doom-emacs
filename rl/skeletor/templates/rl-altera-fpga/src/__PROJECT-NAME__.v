module clk_div (
    input clk_in,
    output reg clk_out
);
parameter div = 2;
reg [31:0]            div_counter = 0;
reg                   prep_clk_out = 0;
always @(posedge clk_in)
begin
    if(div_counter <= div)
        begin
            div_counter <= div_counter + 1;
        end
    else
        begin
            div_counter <= 0;
            prep_clk_out = ~prep_clk_out;
        end
    clk_out <= prep_clk_out;
end
endmodule

module __PROJECT-NAME__
(
    output ds_dp,
    input brd_clk
);

/* CLOCKS */
wire clock_1kHz;
clk_div #(.div(50000)) m_clock_1kHz 
(
    .clk_in(brd_clk),
    .clk_out(clock_1kHz)
);

wire clock_2Hz;
clk_div #(.div(250)) m_clock_2Hz
(
    .clk_in(clock_1kHz),
    .clk_out(ds_dp)
);
endmodule // __PROJECT-NAME__
