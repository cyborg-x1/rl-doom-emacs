"""Implement click example."""

import click


@click.option("--name", prompt="Your name", help="The person to greet.")
def example_click(name):
    """Great persons."""
    click.echo("Hello %s!" % name)
