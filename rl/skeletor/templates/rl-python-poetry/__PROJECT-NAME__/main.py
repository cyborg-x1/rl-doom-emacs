"""Construct and run main application."""

import sys

import structlog
import typer

from SpellTheCheck.cli import app
from SpellTheCheck.cli.example_click import example_click  # noqa: F401

###
# Typer imports "used" by @app.command() decorator in the file
###
from SpellTheCheck.cli.example_typer import example_typer  # noqa: F401

@app.callback(no_args_is_help=True)
def callback():
    """Show help when called with no arguments."""


def main():
    """Construct typer_click interface."""
    log = structlog.get_logger()

    try:
        typer_click = typer.main.get_command(app)
        typer_click.add_command(example_click)
        typer_click()
    except Exception as exception:
        log.fatal("Caught unhandled exception")
        log.exception(exception)
        sys.exit(1)

if __name__ == "__main__":
    main()
