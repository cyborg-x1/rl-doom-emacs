;(defun locate-pyproject-toml
;   (locate-dominating-file (or buffer-file-name default-directory) "pyproject.toml")
;   )

;;; Code:
(require 'flycheck)

(defun python-find-project-toml-root (_checker)
  (locate-dominating-file (or buffer-file-name default-directory) "pyproject.toml"))

(flycheck-define-checker python-poetry-flake8heavened
  "A Python syntax and style checker using Flake8.

Requires Flake8 3.0 or newer. See URL
`https://flake8.readthedocs.io/'."
  ;; Not calling flake8 directly makes it easier to switch between different
  ;; Python versions; see https://github.com/flycheck/flycheck/issues/1055.
  :command ("poetry" "run" "flakeheaven" "lint" "--format" "default" source)
  :standard-input t
  :working-directory python-find-project-toml-root
  :error-filter (lambda (errors)
                  (let ((errors (flycheck-sanitize-errors errors)))
                    (seq-map #'flycheck-flake8-fix-error-level errors)))
  :error-patterns
  ((warning line-start
            (file-name) ":" line ":" (optional column ":") " "
            (id (one-or-more (any alpha)) (one-or-more digit)) " "
            (message (one-or-more not-newline))
            line-end))
  :enabled (lambda ()
             (or (not (flycheck-python-needs-module-p 'python-flake8))
                 (flycheck-python-find-module 'python-flake8 "flake8")))
  :verify (lambda (_) (flycheck-python-verify-module 'python-flake8 "flake8"))
  :modes python-mode
  :next-checkers ((warning . python-pylint)
                  (warning . python-mypy)))

(add-to-list 'flycheck-checkers 'python-poetry-flake8heavened)

(provide 'python-poetry-flake8heavened)
;;; python-poetry-flake8heavened ends here
