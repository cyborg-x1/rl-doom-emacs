(defun rl-delete-row()
  "Delete row like in eclipse"
  (interactive)
  (beginning-of-line)

  (if (/= (line-beginning-position) (line-end-position)) (kill-line))
  (kill-line)
  )

(provide 'rl-delete)
