(require 'rl-cmake-ide)
(require 'rl-move)
(require 'rl-delete)
(require 'rl-menu)


(defun rl-rightmouse(@click)
  (interactive "e")
  (let ((event-name (car @click))
        (event-target-window (posn-window (event-start @click))))
    (select-window event-target-window)
    )
  (let ((p1 (posn-point (event-start @click))))
    (goto-char p1)
    (rl-menu)
  )
)

(defun rl-init-editing-keys-modes()
  "Overwrites mode keys when in list"
  (loop for x in '(c++-mode-hook)
        do
        (add-hook
         x
         (lambda ()
           (local-set-key (kbd "C-d") #'rl-delete-row)
           (local-set-key (kbd "M-<up>") #'rl-move-up)
           (local-set-key (kbd "M-<down>") #'rl-move-down)
           (local-set-key (kbd "C-SPC") #'rl-menu)
           )
         )
        )
  )

;; Init rl-keys
(defun rl-keys-init()
  ;;(rl-init-editing-keys-modes)
  (global-set-key (kbd "C-b") 'rl-cmake-compile)
  (global-set-key (kbd "C-d") 'rl-delete-row)
  (global-set-key (kbd "M-<up>") 'rl-move-up)
  (global-set-key (kbd "M-<down>") 'rl-move-down)
  (global-set-key (kbd "C-<") 'rl-menu)
  (global-set-key (kbd "C-s") 'save-buffer)
  (global-set-key (kbd "C-f") 'isearch-forward)
  (global-unset-key (kbd "C-z"))
  (global-set-key (kbd "C-z") 'undo-tree-undo)
  (global-set-key (kbd "C-a") 'mark-whole-buffer)
  (global-set-key (kbd "C-s-<") 'treemacs)
  (global-set-key (kbd "C-S-z") 'undo-tree-redo)
  ;(global-unset-key  [mouse-3])
  ;(global-set-key [mouse-3] 'rl-rightmouse)
)

(provide 'rl-keys)
