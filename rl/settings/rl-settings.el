
(defun rl-startup()
  (require 'yasnippet)

  (setq neo-window-fixed-size nil)
  ;;(neotree-show)
  ;;(treemacs)
  (yas-exit-all-snippets)
  (setq yas-snippet-dirs '("~/.doom.d/rl/yasnippet/snippets")) ;;set snippet dir
  (yas-reload-all)
  )

(defun rl-apply-settings()
  ;;
  (require 'rl-menu-neotree)
  (require 'rl-menu-treemacs)
  (require 'rl-menu-org)

  ;; Skeletor init
  (add-to-list 'load-path' "~/.doom.d/rl/skeletor")
  (require 'rl-skeletor)
  (rl-skeletor-init)
  ;;;

  ;;; emacs as server
  (server-start)
  ;;;

  ;;; file versioning
  (setq version-control t)
  (setq kept-new-versions 100)
  (setq kept-old-versions 100)
  (setq backup-by-copying t)
  (setq backup-directory-alist `(("." . "~/.doom.d/rl/saves")))
  ;;;

  ;;; copy paste
  (cua-mode t)
  ;;;

  ;;; recent files
  (recentf-mode 1)
  (setq recentf-max-menu-items 100)
  (setq recentf-max-saved-items 100)
  ;;;

  ;;; Display CFS on case fold search
  (add-to-list 'minor-mode-alist '(case-fold-search " CFS "))
  ;;

  (require 'rl-keys)
  (rl-keys-init)

  (require 'rl-cmake-ide)
  (rl-cmake-ide-init)

  (rl-startup)
)

(provide 'rl-settings)

