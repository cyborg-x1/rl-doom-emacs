(require 'rl-delete)
(require 'rl-mark)

(defun rl-move-up-block(start end)
  "moves marked text block one line down by displacing the line below to the top"
  (let (
        (min-line (line-number-at-pos (min start end)))
        (max-line (line-number-at-pos (max start end)))
        )

    (unless (= min-line 1)
       (progn
        (goto-line (- min-line 1))
        (dotimes (m (+ (- max-line min-line) 1))
          (move-text-line-down)
          )
        (rl-mark-lines-by-number (- min-line 1)  (- max-line 1))
       )
    )
  )
)

(defun rl-move-up (start end)
  "Move text up like in eclipse"
  (interactive "r")
  (if (use-region-p)
    (rl-move-up-block start end)
    (move-text-up)
  )
)

(defun rl-move-down-block(start end)
  "moves marked text block one line down by displacing the line below to the top"
  (let (
        (min-line (line-number-at-pos (min start end)))
        (max-line (line-number-at-pos (max start end)))
        (buffer-line-end (line-number-at-pos (buffer-size)))
       )

      (unless (= max-line buffer-line-end)
        (progn
          (goto-line (+ max-line 1))
          (dotimes (m (+ (- max-line min-line) 1))
            (move-text-line-up)
          )
          (rl-mark-lines-by-number (+ min-line 1)  (+ max-line 1))
         )
       )
    )
)


(defun rl-move-down (start end)
  "Move text down like in eclipse"
  (interactive "r")
  (if (use-region-p)
    (rl-move-down-block start end)
    (move-text-line-down)
  )
)

(provide 'rl-move)

