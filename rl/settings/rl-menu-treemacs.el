(defun rl-menu-treemacs-yakuake()
  (interactive)

  (treemacs-block
   (-let [path (treemacs--prop-at-point :path)]
     (treemacs-error-return-if (null path)
       "There is nothing to copy here")
     (treemacs-error-return-if (not (stringp path))
       "Path at point is not a file or directory.")
     (let ()
       (unless (file-directory-p path)
        (setq path (file-name-directory path))
       )
      (shell-command (concat "sh ~/.doom.d/rl/scripts/console.sh " path ))
     )
   )
  )
)


(setq rl-menu-mhashm-treemacs-mode
      #s(hash-table
         test equal
         data (
               "Yakuake" rl-menu-treemacs-yakuake
               )
         )
      )

(provide 'rl-menu-treemacs)
