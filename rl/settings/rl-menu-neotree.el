(require 'rl-menu)
(require 'subr-x)


(defun rl-menu-neotree-console()
    (interactive)
  (shell-command (concat "sh ~/.doom.d/rl/scripts/console.sh " (neo-buffer--get-filename-current-line) ))
)

(setq rl-menu-neotree-goto
      #s(hash-table
         test equal
         data (
               "Home" "~/"
               "Projects" "~/Projects/"
               "Emacs/Private" "~/.doom.d/rl/"
               )
         )
)

(defun rl-neotree-goto()
  (interactive)
  (let (
        (selection)
        (dir)
        )
    (setq selection (rl-menu-from-hashmap rl-menu-neotree-goto))
    (setq dir (gethash selection rl-menu-neotree-goto))
    (neotree-dir dir)
  )
)

(setq rl-menu-mhashm-neotree-mode
      #s(hash-table
         test equal
         data (
               "New" neotree-create-node
               "Rename" neotree-rename-node
               "Delete" neotree-delete-node
               "Open in window" neotree-enter-ace-window
               "Change root to item" neotree-change-root
               "Copy full path" neotree-copy-filepath-to-yank-ring
               "Goto..." rl-neotree-goto
               "Refresh" neotree-refresh
               "Console" rl-menu-neotree-console
               )
         )
      )




(provide 'rl-menu-neotree)
